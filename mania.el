;; mania.el -- a 4k vertically scrolling rhythm game in emacs

;; USER DEFINED CONFIG

(defvar mania-delta-time (/ 1.0 60.0)
  "The time between redrawing each frame")

(defvar mania-note-delay 0.20
  "Note and audio sync has to be done manually here")

(defvar mania-scroll-speed 150.0
  "How fast the notes move up the screen")

(defvar mania-song-file "./audio.mp3"
  "How fast the notes move up the screen")

(defvar mania-beatmap-file "./map.osu"
  "How fast the notes move up the screen")

;; Change keybindings at the bottom of the file
;; Also you're not gonna have a fun time with the garbage collector freezing everything
;; I had somewhat-ok results with (setq gc-cons-threshold (* 8192 5000))
;; EDIT: Ok this ran fairly alright with `emacs -Q`, instead of with everything in Spacemacs

;; Ok don't modify the rest

(defvar *mania-redraw-timer* nil
  "The timer that redraws the screen on a regular basis")

(defvar *mania-media-process* nil
  "The process handle that plays the song audio")

(defvar *mania-start-time* nil
  "The time, in seconds since epoch, that the game started")

(defvar *mania-song-data* nil
  "The active song's hit object data")

(defvar *mania-k1-down* 0
  "Whether key 1 is down")
(defvar *mania-k2-down* 0
  "Whether key 2 is down")
(defvar *mania-k3-down* 0
  "Whether key 3 is down")
(defvar *mania-k4-down* 0
  "Whether key 4 is down")

(defun mania ()
  "Start playing Emacs Mania"
  (interactive)
  (switch-to-buffer "mania")
  (mania-mode)
  (mania-init)
  )

(defun mania-stop ()
  "End a game of Emacs Mania"
  (interactive)
  (cancel-timer *mania-redraw-timer*)
  (interrupt-process *mania-media-process*)
  )

(defun mania-init ()
  "Start a new game of Emacs Mania"
  (setq *mania-song-data* (mania--read-file mania-beatmap-file))
  (setq *mania-media-process* (start-process-shell-command "mania-music" nil (concat "mpv --volume 50 " mania-song-file)))
  (setq *mania-start-time* (float-time))
  (setq *mania-redraw-timer* (run-at-time 0 mania-delta-time 'mania-draw-screen))
  )

(defun mania--read-file (file-path)
  "Reads in the hit objects section of an osu!mania file, returning a list of hit objects in the form ((time-milliseconds key-column) ...)"
  (let ((output-data '()))
    (dolist (line (mania--read-lines file-path))
      (let* ((line-split (split-string line "," t))
             (hit-x (truncate (/ (string-to-number (car line-split)) 128.0)))
             (time-millis (string-to-number (caddr line-split))))
        (add-to-list 'output-data (list time-millis hit-x))))

    output-data))

(defun mania--read-lines (file-path)
  "Return the list of lines in a file at file-path"
  (with-temp-buffer
    (insert-file-contents file-path)
    (split-string (buffer-string) "\n" t)))

(defun mania-draw-screen ()
  "Draw the playfield for Emacs Mania"
  (let ((inhibit-read-only t)
        (current-time (float-time)))
    (erase-buffer)

    ;; Draw the key separators
    (dotimes (y (window-height))
      (insert "                │        │        │        │        │\n"))

    ;; Draw hit objects
    (dolist (obj *mania-song-data*)
      (let* ((time (+ (- (/ (car obj) 1000.0) (mania-get-elapsed-time current-time)) mania-note-delay))
             (y (truncate (* time mania-scroll-speed))))
        (when (and (> y 1) (< y (window-height)))
          (goto-line y)
          (forward-char 17)
          (forward-char (* 9 (cadr obj)))
          (insert (propertize "████████" 'font-lock-face '(:foreground "cyan")))
          (delete-char 8)
          )
        )
      )

    ;; Draw current time
    (goto-char (point-min))
    (end-of-line)
    (insert (format " %f" (mania-get-elapsed-time current-time)))

    ;; Draw notes down
    (when (> *mania-k1-down* 0)
      (goto-line 1)
      (forward-char 17)
      (insert (propertize "████████" 'font-lock-face '(:foreground "red")))
      (delete-char 8)
      (setq *mania-k1-down* (1- *mania-k1-down*))
      )

    (when (> *mania-k2-down* 0)
      (goto-line 1)
      (forward-char (+ 17 9))
      (insert (propertize "████████" 'font-lock-face '(:foreground "red")))
      (delete-char 8)
      (setq *mania-k2-down* (1- *mania-k2-down*))
      )

    (when (> *mania-k3-down* 0)
      (goto-line 1)
      (forward-char (+ 17 18))
      (insert (propertize "████████" 'font-lock-face '(:foreground "red")))
      (delete-char 8)
      (setq *mania-k3-down* (1- *mania-k3-down*))
      )

    (when (> *mania-k4-down* 0)
      (goto-line 1)
      (forward-char (+ 17 27))
      (insert (propertize "████████" 'font-lock-face '(:foreground "red")))
      (delete-char 8)
      (setq *mania-k4-down* (1- *mania-k4-down*))
      )
    )

  (goto-char (point-min))
  )

(defun mania-get-elapsed-time (current-time)
  (- current-time *mania-start-time*))

(defun mania-k1 ()
  "Press the first key"
  (interactive)
  (setq *mania-k1-down* 8)
  ;; (play-sound-file "./out.wav")
  )

(defun mania-k2 ()
  "Press the second key"
  (interactive)
  (setq *mania-k2-down* 8)
  ;; (play-sound-file "./out.wav")
  )

(defun mania-k3 ()
  "Press the third key"
  (interactive)
  (setq *mania-k3-down* 8)
  ;; (play-sound-file "./out.wav")
  )

(defun mania-k4 ()
  "Press the fourth key"
  (interactive)
  (setq *mania-k4-down* 8)
  ;; (play-sound-file "./out.wav")
  )

(define-derived-mode mania-mode special-mode "mania"
  (define-key mania-mode-map (kbd "e") 'mania-k1)
  (define-key mania-mode-map (kbd "r") 'mania-k2)
  (define-key mania-mode-map (kbd "u") 'mania-k3)
  (define-key mania-mode-map (kbd "i") 'mania-k4)
  (define-key mania-mode-map (kbd "q") 'mania-stop)
  )
