Emacs Mania
===========

Because I could.

Emacs Mania lets you take a 4k osu!mania beatmap and play it, within emacs.

<!-- I say somewhat mainly because the garbage collector likes to freeze everything now and then. - I had the best results -->
<!-- with `(setq gc-cons-threshold (* 8192 5000))`. -->

Consider trying `emacs -Q` if this doesn't perform too well. I had issues running this in my Spacemacs setup but a
barebones Emacs setup ran quite well.

![Screenshot](image.png)

## Setup

You'll need

- `mpv` installed to play audio
- A 4k osu!mania map

Take `audio.mp3` from the beatmap and just copy it right next to `mania.el`. Also take the `.osu` file for whatever
difficulty you want to play and put it also next to `mania.el`, renaming it to `map.osu`. Now open `map.osu` in a text
editor and delete *everything* before and including the line `[HitObjects]`. You should be left with just a list of
notes and no timing information/other metadata in the file, like the example below.

```
64,192,124,5,2,0:0:0:0:
448,192,124,1,4,0:0:0:0:
192,192,295,1,0,0:0:0:40:hihat.wav
64,192,466,1,8,0:0:0:0:
320,192,466,1,0,0:0:0:0:
448,192,981,1,2,0:0:0:0:

etc...
```

Aaaand you should be good to go. Open up `mania.el`, evaluate the buffer, then `M-x mania`. If you want to configure
some stuff like scroll speed or sync delay, that's at the top of `mania.el`. Keybindings can be configured at the
bottom of the file.

## Keybindings

- `e r u i` The 4 keys to smash
- `q` Stop playing the song
